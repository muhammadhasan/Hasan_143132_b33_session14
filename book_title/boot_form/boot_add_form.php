
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="well">Book Title Add Form
    <div class="container">

    <form>
        <div class="form-group">
            <label for="email"></label>
            <input type="email" class="form-control" id="email" placeholder="please enter book title here">
        </div>

        <button type="button" class="btn btn-default">Add</button>
        <button type="button" class="btn btn-default">Save & Add</button>
        <button type="button" class="btn btn-default">Reset</button>
        <button type="button" class="btn btn-default">Back to list</button>
    </form>
</div>
</div>

</body>
</html>
